#!/usr/bin/python
#
# Copyright (c) 2019  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
""" Switch from a Yum repo mirror list to a specific repository. """

from __future__ import print_function

import argparse
import io
import os
import re
import subprocess
import sys
import tempfile

try:
    from typing import (
        Any,
        IO,
        List,
        NamedTuple,
        Optional,
        Text,
        Tuple,
    )  # noqa pylint: disable=unused-import

    Config = NamedTuple(
        "Config",
        [("noop", bool), ("verbose", bool)],  # pylint: disable=invalid-name
    )
except ImportError:
    import collections

    Config = collections.namedtuple(  # type: ignore
        "Config", ["noop", "verbose"]
    )

    # Trying very, very hard to avoid a dependency on six,
    # since the very reason for writing this tool is to be able to run it
    # when yum is unable to install a new package...
    #
    if sys.version_info[0] < 3:
        # pylint: disable=undefined-variable
        Text = unicode  # type: ignore  # noqa
    else:
        Text = str  # type: ignore


VERSION_STRING = "0.1.0.dev1"

YUM_REPO_DIR = "/etc/yum.repos.d"


def diag(cfg, msg):
    # type: (Config, str) -> None
    """ Output a diagnostic message if needed. """
    if cfg.verbose:
        print(msg, file=sys.stderr)


def version():
    # type: () -> None
    """ Display program version information. """
    print("yum_switch " + VERSION_STRING)


def features():
    # type: () -> None
    """ Display a list of the features supported by the program. """
    print("Features: yum_switch=" + VERSION_STRING)


def list_repo_files():
    # type: () -> List[str]
    """ Get a list of the Yum repository definition files. """
    repos = sorted(
        name for name in os.listdir(YUM_REPO_DIR) if name.endswith(".repo")
    )
    return [os.path.join(YUM_REPO_DIR, name) for name in repos]


def read_file_lines(fname):
    # type: (str) -> Tuple[List[Text], str]
    """ Try a couple of character encodings and read a file's lines. """
    encodings = ("us-ascii", "UTF-8", "Latin-1")
    for encoding in encodings:
        try:
            return (
                io.open(fname, mode="r", encoding=encoding).readlines(),
                encoding,
            )
        except UnicodeDecodeError:
            pass
    raise Exception(
        "Could not read {fname} as any of {encs}".format(
            fname=fname, encs=", ".join(encodings)
        )
    )


def switch_to_specific(_cfg, mirrorlist, mirror, lines):
    # type: (Config, str, str, List[Text]) -> List[Text]
    """ Switch a mirrorlist to a specific mirror in a set of lines. """
    res = []
    re_list = re.escape(mirrorlist)
    re_tag = re.compile(
        r"""
        ^
        mirrorlist \s* = \s*
        https? ://
    """
        + re_list,
        re.X,
    )
    for line in lines:
        data = re_tag.match(line)
        if not data:
            res.append(line)
            continue

        res.append(
            "### SWITCH {mlist} TO {spec} # {line}".format(
                mlist=mirrorlist, spec=mirror, line=line
            )
        )
        res.append(
            "baseurl=https://{spec}/centos/$releasever/os/$basearch/\n".format(
                spec=mirror
            )
        )

    return res


def switch_to_mirrorlist(_cfg, fname, mirrorlist, lines):
    # type: (Config, str, str, List[Text]) -> List[Text]
    """ Switch a mirrorlist to a specific mirror in a set of lines. """
    res = []
    re_list = re.escape(mirrorlist)
    re_switch = re.compile(
        r"""
        ^
        [#][#][#] \s+ SWITCH \s+ """
        + re_list
        + r"""
        \s+ TO \s+ (?P<spec> \S+ ) \s+
        [#] \s+ (?P<orig> mirrorlist \s* = .* )
        """,
        re.X,
    )
    re_none = re.compile(r""" ^ $ NOMATCH ^ $""")
    re_spec = re_none
    last = None  # type: Optional[Text]
    orig = None  # type: Optional[Text]
    for line in lines:
        if last is not None:
            assert orig is not None
            assert re_spec is not re_none
            data = re_spec.match(line)
            if data:
                res.append(orig + u"\n")
                last = None
                orig = None
                re_spec = re_none
                continue

            # res.append(last)
            # last = None
            # orig = None
            # re_spec = re_none
            # # fallthrough
            raise Exception(
                "Could not restore {fname}: the line following {orig} is "
                "not in the expected format".format(
                    fname=fname, orig=repr(orig)
                )
            )

        assert last is None
        assert orig is None
        assert re_spec is re_none
        data = re_switch.match(line)
        if not data:
            res.append(line)
            continue

        last = line
        orig = data.group("orig")
        re_spec = re.compile(
            r"""
        ^
        baseurl \s* = \s* https? :// """
            + re.escape(str(data.group("spec"))),
            re.X,
        )

    if last is not None:
        assert orig is not None
        assert re_spec is not re_none
        res.append(last)
    else:
        assert orig is None
        assert re_spec is re_none

    return res


def replace_file(fname, tempf):
    # type: (str, IO[Any]) -> None
    """ Replace a real file with the just-generated temporary one. """
    try:
        mode = os.stat(fname).st_mode & 0o7777
    except (IOError, OSError) as err:
        sys.exit(
            "Could not examine {fname}: {err}".format(fname=fname, err=err)
        )
    try:
        os.chmod(tempf.name, mode)
    except (IOError, OSError) as err:
        sys.exit(
            "Could not set the mode of {fname} "
            "to {mode:04o}: {err}".format(fname=fname, mode=mode, err=err)
        )
    try:
        os.rename(tempf.name, fname)
    except (IOError, OSError) as err:
        sys.exit(
            "Could not rename the temporary file {tempf} "
            "to {fname}: {err}".format(tempf=tempf.name, fname=fname, err=err)
        )


def cmd_from(cfg, _cmd, rest):
    # type: (Config, str, List[str]) -> None
    """ Switch from a mirrorlist to a specific repo. """
    if len(rest) != 3 or rest[1] != "to":
        sys.exit("Usage: yum_switch from mirrorlist-url to mirror-url")
    mirrorlist, mirror = rest[0], rest[2]
    diag(
        cfg,
        "Switching from {mlist} to {spec}".format(
            mlist=mirrorlist, spec=mirror
        ),
    )

    for fname in list_repo_files():
        diag(cfg, "Processing {fname}".format(fname=fname))
        lines, encoding = read_file_lines(fname)
        updated = switch_to_specific(cfg, mirrorlist, mirror, lines)
        if updated == lines:
            continue

        diag(cfg, "Changes needed in {fname}".format(fname=fname))
        # Sigh... no "encoding" on Python 2.6, is there?
        with tempfile.NamedTemporaryFile(
            suffix=".switch", prefix="", dir=YUM_REPO_DIR, delete=False
        ) as tempf:
            to_delete = tempf.name  # type: Optional[str]
            try:
                data = Text("".join(updated)).encode(encoding)
                to_write = len(data)
                written = 0
                while written < to_write:
                    res = os.write(tempf.fileno(), data[written:])
                    if res < 1:
                        sys.exit("Could not write to a temporary file")
                    written += res
                os.fsync(tempf.fileno())

                if cfg.noop:
                    subprocess.call(["diff", "-u", "--", fname, tempf.name])
                else:
                    replace_file(fname, tempf)
                    to_delete = None
            finally:
                if to_delete is not None:
                    os.unlink(to_delete)

    diag(cfg, "Nothing more to do, it seems")


def cmd_back(cfg, _cmd, rest):
    # type: (Config, str, List[str]) -> None
    """ Switch from a specific repo back to a mirrorlist. """
    if len(rest) != 2 or rest[0] != "to":
        sys.exit("Usage: yum_switch back to mirrorlist-url")
    mirrorlist = rest[1]
    diag(cfg, "Switching back to {mlist}".format(mlist=mirrorlist))

    for fname in list_repo_files():
        diag(cfg, "Processing {fname}".format(fname=fname))
        lines, encoding = read_file_lines(fname)
        updated = switch_to_mirrorlist(cfg, fname, mirrorlist, lines)
        if updated == lines:
            continue

        diag(cfg, "Changes needed in {fname}".format(fname=fname))
        # Sigh... no "encoding" on Python 2.6, is there?
        with tempfile.NamedTemporaryFile(
            suffix=".switch", prefix="", dir=YUM_REPO_DIR, delete=False
        ) as tempf:
            try:
                to_delete = tempf.name  # type: Optional[str]
                data = Text("".join(updated)).encode(encoding)
                to_write = len(data)
                written = 0
                while written < to_write:
                    res = os.write(tempf.fileno(), data[written:])
                    if res < 1:
                        sys.exit("Could not write to a temporary file")
                    written += res
                os.fdatasync(tempf.fileno())

                if cfg.noop:
                    subprocess.call(["diff", "-u", "--", fname, tempf.name])
                else:
                    replace_file(fname, tempf)
                    to_delete = None
            finally:
                if to_delete is not None:
                    os.unlink(to_delete)

    diag(cfg, "Nothing more to do, it seems")


def main():
    # type: () -> None
    """ The main program: parse arguments, do things. """
    parser = argparse.ArgumentParser(
        prog="yum_switch",
        usage="""
    yum_switch [-Nv] from listhost to repohost
    yum_switch [-Nv] back to listhost
    yum_switch -V | -h | --help | --version
    yum_switch --features

    Examples:
    yum_switch from mirrorlist.centos.org to mirror.example.com
    yum_switch back to mirrorlist.centos.org""",
    )
    parser.add_argument(
        "-N", "--noop", action="store_true", help="no-operation mode"
    )
    parser.add_argument(
        "-V",
        "--version",
        action="store_true",
        help="display program version information and exit",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="verbose operation; display diagnostic output",
    )
    parser.add_argument(
        "--features",
        action="store_true",
        help="display program feature information",
    )
    parser.add_argument("args", nargs="*", help="the action to perform")

    args = parser.parse_args()
    if args.version:
        version()
        sys.exit(0)

    if args.features:
        features()
        sys.exit(0)

    if not args.args:
        parser.print_help(file=sys.stderr)
        sys.exit(1)

    cfg = Config(noop=args.noop, verbose=args.verbose)

    commands = {"back": cmd_back, "from": cmd_from}
    handler = commands.get(args.args[0])
    if not handler:
        parser.print_help(file=sys.stderr)
        sys.exit(1)
    handler(cfg, args.args[0], args.args[1:])


main()
